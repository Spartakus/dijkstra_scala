package hska.wudo.dijkstra.domain

import scala.collection.mutable

package object graph {
  type Graph[N] = N => Map[N, Int]

  case class Point(x: Double, y: Double, var minDistance: Double, var pred: Point)
  case class Edge(in: Point, out: Point, cost: Double = Double.PositiveInfinity)

  class Vertex(cost: Double, start: Point, end: Point) extends Comparable[Vertex] {
    var minDistance: Double = Double.PositiveInfinity

    override def compareTo(other: Vertex): Int = {
      val diff = this.minDistance - other.minDistance
      Math.signum(diff).toInt
    }
  }

  class Dijksta[N](g: Graph[N]) {
    def shortestPath(g: Graph[N])(source: N, target: N):
    Option[List[N]] = {
      val pred = dijkstra1(g)(source)._2
      if (pred.contains(target) || source == target)
        Some(iterateRight(target)(pred.get))
      else None
    }

    def iterateRight(x: N)(f: N => Option[N]): List[N] = {
      def go(x: N, acc: List[N]): List[N] = f(x) match {
        case None => x :: acc
        case Some(y) => go(y, x :: acc)
      }

      go(x, List.empty)
    }

    def dijkstra1(g: Graph[N])(source: N): (Map[N, Int], Map[N, N]) = {
      val active = mutable.Set(source)
      val res = mutable.Map(source -> 0)
      val pred = mutable.Map.empty[N, N]
      while (active.nonEmpty) {
        val node = active.minBy(res)
        active -= node
        val cost = res(node)
        for ((n, c) <- g(node)) {
          val cost1 = cost + c
          if (cost1 < res.getOrElse(n, Int.MaxValue)) {
            active += n
            res += (n -> cost1)
            pred += (n -> node)
          }
        }
      }
      (res.toMap, pred.toMap)
    }
  }

}
