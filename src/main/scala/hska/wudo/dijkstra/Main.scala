package hska.wudo.dijkstra

import hska.wudo.dijkstra.Dijkstra.{Edge, Graph, Node}

import scala.annotation.tailrec
import scala.collection.immutable.SortedSet
object Dijkstra {

  case class Node(name: String, x:Double = 0, y:Double = 0, minDistance: Double = Double.PositiveInfinity, precessor: Option[Node] = None) extends  Ordered[Node] {
    def compare(that: Node) = (this.minDistance - that.minDistance).toInt
  }
  case class Edge(from: Node, to: Node, distance: Double)
  case class Graph(nodes:List[Node], edges:List[Edge])

  case class UpdateNode(newNode: Node, oldNode: Node)

  def Dijkstra(g:Graph)(start:Node):Graph = {

    val initialSet: SortedSet[Node] = SortedSet(start)
    val newStartNode = start.copy(minDistance =  0)
    val initialGraph = updateNode(g)(newStartNode)(start)

    @tailrec
    def calculateDistances(g:Graph)(toBeDiscovered:List[Node])(visitedNodes:List[Node]): Graph = {

      if (toBeDiscovered.isEmpty) return g

      val nextNode = toBeDiscovered.min
      val nextNodeDistance = nextNode.minDistance

      val directConnections = getConnections(g)(nextNode).filter { case(edge,node) => !visitedNodes.contains(node) }

      val filteredConnections = directConnections.filter { case(edge,node) => nextNodeDistance + edge.distance < node.minDistance }
      val correctedNodes = filteredConnections.map { case(edge,node) => node.copy( minDistance = nextNodeDistance + edge.distance,  precessor = Some(nextNode) ) }.toList

      val updatePairs = filteredConnections.map { case(edge,node) => node }
                        .zip( correctedNodes )
                        .map{ e => UpdateNode(newNode = e._2, oldNode = e._1) }
                          .toList

      val newGraph = updateNodes(g)(updatePairs)

      val newDiscList = correctedNodes ++ toBeDiscovered
      val newDiscoveredFiltered = newDiscList filter ( _ != nextNode )
      val newVisited = visitedNodes.::( nextNode )
      //val newVisitedCorrected = newVisited.filter( x  => correctedNodes.contains(x) )

      calculateDistances(newGraph)(newDiscoveredFiltered)(newVisited)

    }

    calculateDistances(initialGraph)(List(newStartNode))(Nil)
  }

  @tailrec
  def updateNodes(g:Graph)(pairs:List[UpdateNode]): Graph = {
    pairs match {
      case Nil => g
      case (n:UpdateNode) :: rest =>
        val newgraph = updateNode(g)(n.newNode)(n.oldNode)
        updateNodes(newgraph)(rest)

    }
  }

  def updateNode(g:Graph)(newNode: Node)(oldNode: Node): Graph = {

    @tailrec
    def updateNodes(g:Graph)(nodes: List[UpdateNode]): Graph = {
      nodes match {
        case Nil =>  g
        case (x: UpdateNode) :: (rest: List[UpdateNode]) =>

          val oldInEdges = g.edges.filter( e => e.from == oldNode  )
          val oldOutEdges = g.edges.filter( e => e.to == oldNode  )
          val newInEdges = oldInEdges.map( e => e.copy(from=newNode)  )
          val newOutEdges = oldOutEdges.map( e => e.copy(to=newNode)  )

          val oldSuccessors = g.nodes.filter( ne => ne.precessor.contains(oldNode))
          val newSuccessors = oldSuccessors.map( s => s.copy(precessor = Some(newNode)) )

          val newEdges = g.edges.diff(oldInEdges).diff(oldOutEdges) ++ newInEdges ++ newOutEdges
          val newNodes = g.nodes.filter( n => n != oldNode).+:(newNode)

          val newGraph =  g.copy( edges = newEdges, nodes = newNodes )

          val newUpdateNodes = oldSuccessors.zip(newSuccessors).map( x => UpdateNode(x._2, x._1) )

          updateNodes(newGraph)(rest ++ newUpdateNodes)
      }
    }

    updateNodes(g)(List(UpdateNode(newNode,oldNode)))

  }
/*
  def pickShortestNext(neighbours:Map[Edge,Node]): (Edge,Node) = {
    neighbours.min( Ordering.by { case(k,v) => v.minDistance } )
  }
*/
  def getAdjacentNodes(g:Graph)(n:Node): List[Node] = {
    val in = g.edges.filter( e => e.from == n  ).map( e => e.to )
    val out = g.edges.filter( e => e.to == n  ).map( e => e.from )
    in ++ out
  }

  def getConnections(g:Graph)(n:Node): Map[Edge, Node] = {
    val in = g.edges.filter( e => e.from == n  )
    val inNodes = in.map( e => e.to )
    val zipIn = in.zip(inNodes)
    val mapIn = zipIn.toMap

    val out = g.edges.filter( e => e.to == n  )
    val outNodes = out.map( e => e.from )
    val zipOut = out.zip(outNodes)
    val mapOut = zipOut.toMap

    mapIn ++ mapOut
  }

  def printGraph(g:Graph): Unit = {

    @tailrec
    def printNode(g: Graph)(n:List[Node])(nDone:List[Node]): Unit = {
      if (nDone == g.nodes || n.isEmpty) return

      val cur = n.head
      val connections = getConnections(g)(cur)

      println(s"Node ${cur.name} | has ${connections.size} Edges | ${cur.minDistance}")
      cur.precessor.foreach { x => println(s"> Precessor is ${x.name}") }


      for ( (key, value) <- connections.filter( x => !nDone.contains(x._2) ) ) {
        println (s"Node ${cur.name} ~ ${value.name}")
      }

      val newNodes = connections.map { case (k,v) => v }   .filter( x => !nDone.contains(x) ).toList

      val newDone = nDone ++ List(cur)

      println("-----------------")
      printNode(g)(newNodes)(newDone)
    }

    printNode(g)(List(g.nodes.head))(Nil)
  }

  //def shortestPath(g: Graph)(start:Node)(end:Node) = _
}

object Main extends App {
/*
  val graphAsDGS =
    """
       |DGS004
       |triangle 0 6
       |an A ui.label=A
       |an B ui.label=B
       |an C ui.label=C
       |an D ui.label=D
       |an E ui.label=E
       |an F ui.label=F
       |ae AB A B ui.label=11
       |ae AC A C ui.label=23
       |ae BD B D ui.label=2
       |ae CE C E ui.label=13
       |ae DF D F ui.label=20
       |ae EF E F ui.label=5
    """.stripMargin.trim

  val streamDHS = new StringReader(graphAsDGS)
  val filesource = new FileSourceDGS

  val graph = new SingleGraph("Test")

  filesource.addSink(graph)
  filesource.readAll(streamDHS)
  graph.display()*/

  val NodeA = Node("Node A", 1, 1)
  val NodeB = Node("Node B", 1, 2)
  val NodeC = Node("Node C", 2, 1)
  val NodeD = Node("Node D", 2, 2)
  val NodeE = Node("Node E", 3, 1)
  val NodeF = Node("Node F", 3, 2)

  val nodes = List(NodeA, NodeB, NodeC, NodeD, NodeE, NodeF)

  val EdgeAB = Edge(NodeA, NodeB, 7)
  val EdgeAC = Edge(NodeA, NodeC, 1)
  val EdgeBD = Edge(NodeB, NodeD, 1)
  val EdgeCE = Edge(NodeC, NodeE, 1)
  val EdgeDF = Edge(NodeD, NodeF, 1)
  val EdgeEF = Edge(NodeE, NodeF, 1)

  val edges = List(EdgeAB, EdgeAC, EdgeBD, EdgeCE, EdgeDF, EdgeEF)

  val graph = Graph(nodes, edges)


  val newNodeB = Node("Node B NEW!", 2, 3)
  val newGraph = Dijkstra.updateNode(graph)(newNodeB)(NodeB)

  Dijkstra.printGraph(graph)
  println("--- Neeext ---")
  Dijkstra.printGraph(newGraph)

  println("--- Neeext ---")
  println("--- Neeext ---")
  println("--- Neeext ---")

  val calcGraph = Dijkstra.Dijkstra(newGraph)(newNodeB)
  Dijkstra.printGraph(calcGraph)


}
