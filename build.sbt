name := "Dijkstra"

version := "0.1"

scalaVersion := "2.12.3"
libraryDependencies += "org.graphstream" % "gs-core" % "1.3"
libraryDependencies += "org.graphstream" % "gs-ui" % "1.3"
libraryDependencies += "org.graphstream" % "gs-algo" % "1.3"
